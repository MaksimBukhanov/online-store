import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from 'react-redux';
import "@fontsource/poiret-one"
import store from "./redux/redux-store";

export let renderApp = () => {
    ReactDOM.render(
     <Provider store = {store}>
      <App />,
     </Provider>,
    document.getElementById('root')
  )
}
renderApp();
//store.subscribe(renderApp);
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
