import React from "react";
import classes from './Button.module.css';
import {Link} from "react-router-dom";

const Button = (props) => {
    let style = {
        link: props.link,
        width: `${props.width}px`,
        height: `${props.height}px`,
        margin: props.margin,
        padding: props.padding,
        borderRadius: `${props.radius}`,
        textAlign: props.textAlign
    }
    return (
          <Link to={props.link} className={props.light? classes.button_white : classes.button_black} style={style}>{props.caption}</Link>
    )
}

export default Button;