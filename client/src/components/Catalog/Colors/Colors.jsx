import React, {createRef, useEffect, useState} from "react";
import classes from './Colors.module.css';


const Colors = (props) => {
    let [flag, setFlag] = useState(false);
    let target = createRef();
    useEffect(()=> {
        if (flag) {
            target.current.style.fontWeight = "bold";
        }
        else {
            target.current.style.fontWeight = "normal";
        }
    }, [flag])
    return (
        <div className={classes.color_wrapper} ref={target} onClick={() => setFlag(!flag)}>
             <div style={{background: props.hex}} className={ props.name ? classes.color : classes.color_big}/>
            {props.name ? <div className={classes.color_name}>{props.name}</div> : '' }

        </div>
    )
}

export default Colors;