import React, {createRef, useEffect, useState} from "react";
import classes from './Sizes.module.css';


const Sizes = (props) => {
    let activeStyle = {backgroundColor: "#000", color: "#fff"}
    let [flag, setFlag] = useState(false);
    let target = createRef();
    useEffect(()=> {
        if (flag) {
            target.current.style.backgroundColor = activeStyle.backgroundColor;
            target.current.style.color = activeStyle.color;
        }
        else {
            target.current.style.backgroundColor = target.current.style.color = "";
        }
    }, [flag])
    return (
        <div>
            <div className={classes.size} ref={target} onClick={()=> setFlag(!flag)}>{props.size}</div>
        </div>
    )
}

export default Sizes;