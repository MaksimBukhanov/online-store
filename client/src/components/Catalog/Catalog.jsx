import React, {createRef, useEffect, useState} from "react";
import classes from './Catalog.module.css';
import logo from '../../img/logo.png';
import {Link, NavLink, useMatch} from "react-router-dom";
import {colors, detailItems, menuItems, pagination, razmers, routes} from "./helpers";
import {useParams} from "react-router-dom";
import Item from "./Detail/Item/Item";
import Master from "./Master/Master";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {setDetailActionCreator, setEmptyActionCreator, setReloadingActionCreator} from "../../redux/catalogReducer";
import Preloader from "../Loader/Loader";

const Catalog = (props) => {
    let params = useParams();
    let dispatch = useDispatch();
    let [pages, setPages] = useState([]);
    let [currentPage, setCurrentPage] = useState(1);
    let currentDirection, currentGender, type;
    let [isLoading, setIsLoading] = useState(false);
    let [isEmp, setEmp] = useState(true);
    let colors = useSelector(state => state.catalog.filter?.colors ? state.catalog.filter?.colors : []  );
    let sizes = useSelector(state => state.catalog.filter?.sizes ? state.catalog.filter?.sizes : [] );
    let sourceItems = useSelector(state => state.catalog?.detailItems);
    let categories = useSelector(state => state.catalog.subCategories);
    let isNeedReloading = useSelector(state => state.catalog.needReloading);
    let tip = '';
    let [source, setSource] = useState([]);
    let checkPage = (totalPage) => {
        let pgs = [];
        for (let page = 1; page <= totalPage; page++) {
            pgs.push(page);
        }
        return pgs;
    }
    let changePage = (page) => {
        setCurrentPage(page);
        dispatch(setReloadingActionCreator(true));
    }
    const master = createRef();
    const detail = createRef();
    routes.forEach(route => {
        if (route.path === params.gender) {
            type = params.type === 'clothes' ? '1' : '2'
            currentDirection = route.name;
            currentGender = route.id;
        }
    })
    if (params.category) {
        let href = '/' + params.type + '/' + params.category;
        categories.forEach(category => {
            if (category.href === href) {
                tip = `&type=${category.id}`
            }
        })
    }
    useEffect(() => {
        if (!sourceItems?.length || isNeedReloading) {
            setIsLoading(true);
            if (isNeedReloading) dispatch(setReloadingActionCreator(false));
            axios.post(`http://185.7.84.163:8080/api/catalog/products?class=${type}&gender=${currentGender}&page=${currentPage-1}&size=6` + tip, {colors, sizes})
                .then(res => {
                    setSource(res.data);
                    setEmp(false);
                    setPages(checkPage(res.data.totalPage));
                    setIsLoading(false);
                })
                .catch(e => {
                    if (e.response.status === 404) {
                        setSource(null);
                        setEmp(true);
                        setIsLoading(false);
                    }
                })
        }
    }, [params, isNeedReloading]);
    const showMaster  = (event) => {
        if (window.screen.width < 768) {
            if (master.current.style.display == 'none') {
                master.current.style.display = 'flex';
                detail.current.style.display = 'none';
            } else {
                master.current.style.display = 'none';
                detail.current.style.display = 'block';
            }
        }
    }
    if (isLoading) return  <Preloader/>
    return (
        <div className={classes.catalog_container}>
            <div onClick={(event) => showMaster(event)} className={classes.filter_icon}/>
            <div  className={classes.catalog_title_container}>
                <h1 className={classes.catalog_title}>Каталог</h1>
            </div>
            <div className={classes.catalog_masterdetail}>
                <div className={classes.catalog_master} ref={master}>
                    <Master params={params} showMaster={showMaster} currentDirection={currentDirection}/>
                </div>
                <div className={classes.catalog_detail} ref={detail}>
                    {isEmp ? <div className={classes.catalog_container_empty}>
                                     <div className={classes.catalog_container_empty_wrapper}>
                                     <div className={classes.catalog_container_empty_title}>
                                        <div className={classes.catalog_container_empty_image}/>
                                            Товаров не найдено
                                        </div>
                                     </div>
                                </div> :  <div className={classes.catalog_detail_container}>
                                        <div className={classes.catalog_detail_items}>
                                            {source?.page?.map(item => <Item config={item}/>)} </div>
                                    <div className={classes.catalog_detail_pagination}>
                            {pages.map(page => <div onClick={() => changePage(page)} className={classes.catalog_detail_pagination_item}>{page}</div>)}
                                    </div>
                                    </div>
                    }
                </div>
            </div>
        </div>
    )
}

export default Catalog;