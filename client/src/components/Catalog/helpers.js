export let routes = [
    {path: 'man', name: 'Для мужчин', id: '1'},
    {path: 'woman', name: 'Для женщин', id: '2'},
    {path: 'girl', name: 'Для девочек', id: '3'},
    {path: 'boy', name: 'Для мальчиков', id: '4'}
]


export let menuItems = [
    {path: 'yubki', name: 'Джинсы'},
    {path: 'shirts', name: 'Футболки'},
    {path: 'dress', name: 'Шорты'},
    {path: 'shorts', name: 'Рубашки'},
    {path: 'belie', name: 'Штаны'},
    {path: 'belie', name: 'Худи'},
    {path: 'belie', name: 'Жилеты'}
]

export let razmers = [
    "40-42","42-44","44-46","48-50","52-54","54-56","56-58"]

export let colors = [{name: 'red', title: 'Красный'},
    {name: 'blue', title: 'Голубой'},
    {name: 'white', title: 'Белый'},
    {name: 'green', title: 'Зеленый'},
    {name: 'black', title: 'Черный'}]

export let detailItems = [
    {name: "Шорты мужские", price: 999, img: 'https://storage.vsemayki.ru/images/0/1/1645/1645225/previews/people_6_man_shorts_sport_front_white_500.jpg'},
    {name: "Рубашка мужская", price: 1499, img: 'https://wlooks.ru/images/article/thumb/450-0/2016/08/kak-pravilno-zapravlyat-rubashku--63.jpg'},
    {name: "Футболка мужская", price: 499, img: 'https://thumbs.dreamstime.com/b/пустая-бе-ая-футбо-ка-на-те-е-s-че-овека-88991949.jpg'},
    {name: "Худи", price: 3999, img: 'https://lamcdn.net/wonderzine.com/post_image-image/NLVM2v29RKb1kMwQ4MQ_tg-small.jpg'},
    {name: "Брюки утепленные", price: 2999, img: 'https://telegra.ph/file/6a79a10966153d023ce71.png'},
    {name: "Жилет мужской", price: 4999, img: 'https://gd4.alicdn.com/imgextra/i2/70591677/O1CN01l8tFbB1OG6j8iLGEQ_!!70591677.jpg'}
]

export let pagination = [1, 2, 3, 4, 5, 6];