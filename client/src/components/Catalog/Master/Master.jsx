import React, {useEffect, useState} from "react";
import classes from './Master.module.css';
import {NavLink} from "react-router-dom";
import {colors, menuItems, razmers} from "../helpers";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {setFilterActionCreator, setItemsActionCreator, setReloadingActionCreator} from "../../../redux/catalogReducer";
import Sizes from "../Sizes/Sizes";
import Colors from "../Colors/Colors";
import Preloader from "../../Loader/Loader";

const Master = (props) => {
    const dispatch = useDispatch();
    const [isLoading, setLoading] = useState(false);
    useEffect(() => {
        setLoading(true);
        const params = props.params.type ? `&clazz=` + props.params.type : '';
        axios.get(`http://185.7.84.163:8080/api/catalog/filter?gender=${props.params.gender}${params}`)
            .then(response => {
                dispatch(setItemsActionCreator(response.data))
                setLoading(false);
            })
    }, [props.params.type]);
    const items = useSelector((state) => state.catalog);
    let currentSizes = useSelector(state => state.catalog?.filter?.sizes ? state.catalog.filter.sizes : []);
    let currentColors = useSelector(state => state.catalog?.filter?.colors ? state.catalog?.filter?.colors : []);
    const getItems = () => {
        dispatch(setReloadingActionCreator(true));
        props.showMaster();
    }
    const onClickHandler = (event, type, id) => {
        switch (type) {
            case "size": {
                if (!currentSizes.includes(id)) {
                    currentSizes.push(id);
                    dispatch(setFilterActionCreator({size: currentSizes}));
                }
                else {
                    const index = currentSizes.indexOf(id);
                    if (index > -1) currentSizes.splice(index, 1);
                }
                dispatch(setReloadingActionCreator(true));
                break;
            }
            case "color": {
                if (!currentColors.includes(id)) {
                    currentColors.push(id);
                    dispatch(setFilterActionCreator({color: currentColors}));
                }
                else {
                    const index = currentColors.indexOf(id);
                    if (index > -1) currentColors.splice(index, 1);
                }
                dispatch(setReloadingActionCreator(true));
                break;
            }
        }
    }
    if (isLoading) return <Preloader/>
    return (
        <div>
            <ul className={classes.master_menu_wrapper}><NavLink className={({isActive}) => isActive ? classes.master_menu_active : classes.master_menu} to={`/catalog/${props.params.gender}/${props.params.type}`} end>{props.currentDirection}</NavLink>
                {items.subCategories.map(item => <li className={classes.master_menu_item} onClick={() => getItems()}>
                        <NavLink className={({isActive}) => isActive ? classes.master_menu_item_title_active : classes.master_menu_item_title}
                                 to={`/catalog/${props.params.gender}${item.href}`} end>{item.name}</NavLink>
                    </li>
                )}
            </ul>
            <div className={classes.master_menu_wrapper}><span className={classes.master_menu_active}>Размеры</span>
                <div className={classes.master_menu_sizes}>{items.sizes.map(size => <div onClick={(event) => onClickHandler(event, "size", size.id) }><Sizes size={size.size}/></div>)}</div>
            </div>
            <div className={classes.master_menu_wrapper}><span className={classes.master_menu_active}>Цвета</span>
                <div className={classes.master_menu_colors}>{items.colors.map(color => {
                    return (
                        <div className={classes.color_container} onClick={(event) => onClickHandler(event, "color", color.id) }> <Colors name={color.name} hex={color.hex}/>
                        </div> )
                })}
                </div>
            </div>
            <div onClick={(() => props.showMaster())} className={classes.master_menu_wrapper_mobile}><span className={classes.master_menu_active}>Применить</span></div>
        </div>
    )
}

export default Master;