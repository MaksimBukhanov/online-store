import favIcon from "../../img/favorites.svg";
import cartIcon from "../../img/cart.svg";
import profileIcon from "../../img/profile.svg";

export const getItem = (item) => {

    let image;
    switch (item) {
        case "fav": image = favIcon; break;
        case "cart": image = cartIcon; break;
        case "profile": image=profileIcon; break;
    }
    return {
        backgroundImage: `url(${image})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'contain'
    }
}

export let menuItemsFromData = [
    {
        title: "Для мужчин",
        href: '/men',
        class: [
            {
                "name": "Обувь",
                "href": 2,
                "items": [
                    {
                        "name": "Ботинки",
                        "img": null,
                        "href": 7
                    }
                ]
            },
            {
                "name": "Одежда",
                "href": 1,
                "items": [
                    {
                        "name": "Джинсы",
                        "img": null,
                        "href": 1
                    }
                ]
            }
        ]
    },
    {
        title: "Для женщин",
        href: "/women",
        class: [
            {
                "name": "Обувь",
                "href": null,
                "items": []
            },
            {
                "name": "Одежда",
                "href": 1,
                "items": [
                    {
                        "name": "Джинсы",
                        "img": null,
                        "href": 2
                    },
                    {
                        "name": "Юбка",
                        "img": null,
                        "href": 5
                    }
                ]
            }
        ]
    },
    {
        title: "Для мальчиков",
        href: '/boys',
        class: [
            {
                name: "Обувь",
                href: 2,
                items: [
                    {
                        name: "Ботинки",
                        img: null,
                        href: 8
                    }
                ]
            },
            {
                name: "Одежда",
                href: 1,
                items: [
                    {
                        name: "Джинсы",
                        img: null,
                        href: 3
                    }
                ]
            }
        ]
    },
    {
        title: "Для девочек",
        href: '/girls',
        class: [
            {
                name: "Обувь",
                href: null,
                items: []
            },
            {
                name: "Одежда",
                href: 1,
                items: [
                    {
                        name: "Джинсы",
                        img: null,
                        href: 4
                    },
                    {
                        name: "Юбка",
                        img: null,
                        href: 6
                    }
                ]
            }
        ]
    }
];
