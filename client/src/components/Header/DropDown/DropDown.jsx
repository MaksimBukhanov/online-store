import React from "react";
import classes from './DropDown.module.css';
import {NavLink} from "react-router-dom";

const DropDown = (props) => {
    let items = [];
    let currentLocation;
    for (let key in props.data) {
        if (props.data[key]?.title === props?.currentValue) {
            currentLocation = props.data[key]?.href
            items = props.data[key]?.items;
        }
    }
    return (
        <div className={classes.dropdown}>
            {items?.map(category =>  <div key={category.name} className={classes.dropdown_column}>
                <NavLink className={classes.dropdown_title} to={'/catalog' + currentLocation + category.href}> {category.name}
                </NavLink>
                <div className={classes.dropdown_items}>
                {category.items.map(item => <div key={item.name} className={classes.dropdown_item_container}><NavLink className={classes.dropdown_item} to={'/catalog' + currentLocation + category.href + item.href}> {item.name}
                </NavLink></div>)}
                </div>

            </div>)}
        </div>

    )
}

export default DropDown;