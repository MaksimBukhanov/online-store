import React, {createRef, useEffect, useState} from "react";
import classes from './Header.module.css'
import {getItem, menuItemsFromData} from "./helpers";
import {Link, NavLink} from "react-router-dom";
import DropDown from "./DropDown/DropDown";
import {
    resetSearchActionCreator,
    setCartActionCreator,
    setFavActionCreator,
    setProfileActionCreator, setSearchActionCreator,
    setShowingActionCreator
} from "../../redux/headerReducer";
import {connect, useDispatch, useSelector} from "react-redux";
import axios from "axios";

let mapDispatchToProps = (dispatch) => {
    return {
        updateCount: (type, data) => {
            switch (type) {
                case "fav": dispatch(setFavActionCreator(data)); break;
                case "cart": dispatch(setCartActionCreator(data)); break;
                case "profile": dispatch(setProfileActionCreator(data)); break;
            }
        },
        setShowing: (event, flag) => {
            let currentValue = event.currentTarget?.innerText;
            dispatch(setShowingActionCreator({currentValue, flag}))
        }
    }
}
let mapStateToProps = (state) => {
    return {
        counts: state.header.counts,
        isShowing: state.header.isShowing,
        currentValue: state.header.currentValue
    }
}
const Header = (props) => {
    let [menuItemsFromServer, setMenuItemsFromServer] = useState([]);
    let dispatch = useDispatch();
    let searchValue = useSelector(state => state.header.searchString);
    useEffect(() => {
        if (!menuItemsFromServer.length) {
            axios.get("http://185.7.84.163:8080/api/categories/all")
                .then(result => setMenuItemsFromServer(result.data))
                .catch(e => console.log(e))
        }
    }, []);
    let onChangeHandler = (event) => {
        dispatch(setSearchActionCreator(event.target.value));

    }
    let showSearch = () => {
        if (searchValue) dispatch(resetSearchActionCreator());
        if (mobileHeader.current.style.display !== 'none') {
            mobileHeader.current.style.display = 'none';
            mobileSearch.current.style.display = 'flex'
        }
        else {
            mobileHeader.current.style.display = 'flex';
            mobileSearch.current.style.display = 'none'
        }
    }
    let mobileMenu = createRef();
    let mobileHeader = createRef();
    let mobileSearch = createRef();
    let showMenu = () => {
        if (mobileMenu.current.style.display === 'none') {
            mobileMenu.current.style.display = 'block'
        }
        else {
            mobileMenu.current.style.display = 'none'
        }
    }
    return (
        <div className={classes.header}>
            <div className={classes.header_main} onMouseEnter={() => props.isShowing ? props.setShowing(false) : ''}>
                <div className={classes.header_mobile}>
                    <div className={classes.header_mobile_area} ref={mobileHeader}>
                        <div onClick={() => showMenu()} className={classes.header_mobile_menu}/>
                        <div>
                            <NavLink to='/' className={classes.header_logo_image_mobile}>
                                <div className={classes.header_logo_mobile}/>
                            </NavLink>
                        </div>
                        <div className={classes.header_links_mobile}>
                            <div onClick={() => showSearch() } className={classes.header_search_button}/>
                            <Link to='/profile' className={classes.header_links_item} style={getItem('profile')}/>
                        </div>
                    </div>
                    <div className={classes.header_search_mini} ref={mobileSearch}>
                        <input className={classes.header_search__input} onChange={(event) => onChangeHandler(event)} value = {searchValue} placeholder="Поиск" type="text"/>
                        <span onClick={() => showSearch()} className={classes.header_search_clean}>x</span>
                    </div>
                </div>
                <div className={classes.header_search}>
                    <input className={classes.header_search__input} onChange={(event) => onChangeHandler(event)}  value = {searchValue} placeholder="Поиск" type="text"/>
                    <div className={classes.header_search_button}/>
                </div>
                <NavLink to='/' className={classes.header_logo_image}>
                    <div className={classes.header_logo}/>
                </NavLink>
                <div className={classes.header_links}>
                    <Link to = '/favorites' className={classes.header_links_item_fav}>
                        <div className={classes.header_links_item_count}>
                            {props.counts.fav ?
                                <span className={classes.header_links_item_count_value}>{props.counts.fav}</span> : ''}
                        </div>
                    </Link>
                    <Link to='/cart' className={classes.header_links_item} style={getItem('cart')}>
                        <div className={classes.header_links_item_count}>
                            {props.counts.cart ?
                                <span className={classes.header_links_item_count_value}>{props.counts.cart}</span> : ''}
                        </div>
                    </Link>
                        <Link to='/profile' className={classes.header_links_item} style={getItem('profile')}>
                            <div className={classes.header_links_item_count}>
                                {props.counts.profile ? <span
                                    className={classes.header_links_item_count_value}>{props.counts.profile}</span> : ''}
                            </div>
                        </Link>
                </div>
            </div>
            <div className={classes.header_menu}>
                <div className={classes.header_menu_items}>
                    <ul className={classes.header_menu_list}>
                        {menuItemsFromServer.map(item => <NavLink
                                to={'#'}
                                onMouseEnter={(event) => props.setShowing(event, true)}
                                className={classes.header_menu_items}
                                key={item.title}> {item.title}
                            </NavLink>
                        )}
                    </ul>
                </div>
            </div>
            <div className={classes.header_menu_dropdown_container}>
                <div className={classes.header_menu_dropdown}
                     style={{display: props.isShowing ? 'block' : 'none', position: 'absolute'}}
                     onMouseLeave={() => props.isShowing ? props.setShowing(false) : ''}>
                    <DropDown currentValue={props.currentValue} data={menuItemsFromServer}/>
                </div>
            </div>
            <div ref={mobileMenu} className={classes.mobile_menu}>
                <div className={classes.menu_title}>Каталог</div>
                <div className={classes.menu_subcategories}>
                {menuItemsFromServer.map(category => {
                    return (
                    <div onClick={() => showMenu()} className={classes.menu_subtitle}> <Link to={`/catalog${category.href}/clothes`} className={classes.menu_link}>{category.title}</Link>
                    </div>
                    )
                })}
                </div>
                <div onClick={() => showMenu()} className={classes.menu_title}><Link to = {'/favorites'} className={classes.menu_link}>Избранное </Link></div>
                <div onClick={() => showMenu()} className={classes.menu_title}><Link to = {'/cart'} className={classes.menu_link}>Корзина</Link></div>
            </div>
        </div>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);