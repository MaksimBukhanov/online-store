import React, {useEffect, useState} from "react";
import classes from './AuthForm.module.css';
import {Link, Navigate, useLocation} from "react-router-dom";
import API from "../../../api/api";
import axios from "axios";
import jwt_decode from 'jwt-decode';
import {useDispatch, useSelector} from "react-redux";
import {setAdminActionCreatore, setAuthActionCreator, setUserIdActionCreator} from "../../../redux/authReducer";

;

const AuthForm = (props) => {
    let [email, setEmail] = useState('');
    let [name, setName] = useState('');
    let [password, setPassword] = useState('');
    let [pass, setPass] = useState('');
    let isRegistration = useLocation().pathname !== '/signup'
    let [isSamePasswords, setSamePasswords] = useState(false);
    let [isFirstInput, setFirstInput] = useState(true);
    const dispatch = useDispatch();

    useEffect(()=> {
        if (isRegistration) setSamePasswords (pass === password);
    }, [pass, password])
    useEffect(()=> {
        setEmail('');
        setPass('');
    }, [isRegistration])
    const isAuth = useSelector(state => state.auth.isAuth);
    if (isAuth) return  <Navigate to='/profile' replace/>

    const onChangeHandler = (event, type) =>  {
        switch (type) {
            case 'name': setName(event.target.value); break;
            case 'email': setEmail(event.target.value); break;
            case 'password': setPassword(event.target.value); break;
            case 'pass': {
                setPass(event.target.value);
                setFirstInput(false);
                break;
            }
        }
    }


    const handleSubmit = (e) => {
        e.preventDefault();
        if (isRegistration) {
            axios.post('http://185.7.84.163:8080/api/client/reg', {email: email, fullName: name, password: password, phone: '+799999999', role: 1})
                .then(response => {
                    localStorage.setItem('token', response.data.accessToken);
                    localStorage.setItem('refreshToken', response.data.refreshToken);
                    const token = response.data.accessToken;
                    const info = jwt_decode(token);
                    if (info.roles === 2) dispatch(setAdminActionCreatore(true));
                    dispatch(setUserIdActionCreator(+info.id));
                    dispatch(setAuthActionCreator(true))
                });
        }
        else {
            axios.post('http://185.7.84.163:8080/api/auth/login', {email, password})
                .then(response => {
                    localStorage.setItem('token', response.data.accessToken)
                    localStorage.setItem('refreshToken', response.data.refreshToken);
                    const token = response.data.accessToken;
                    const info = jwt_decode(token);
                    if (info.roles === 2) dispatch(setAdminActionCreatore(true));
                    dispatch(setUserIdActionCreator(+info.id));
                    dispatch(setAuthActionCreator(true))
                });
        }
    }

    return (
        <div className = {classes.auth_container}>
            <h1 className={classes.auth_main_title}>{isRegistration? 'Регистрация' : 'Авторизация'}</h1>
                    <form onSubmit={handleSubmit}>
                        {isRegistration? <label className={classes.auth_label}> Как вас зовут?
                            <input className={classes.auth_input} placeholder="Иванов Иван Иванович" type="text" value={name} onChange={(event) => onChangeHandler(event, 'name')}/>
                        </label> : ''}
                        <label className={classes.auth_label}> Адрес эл.почты
                        <input className={classes.auth_input} placeholder="Введите email" type="email" value={email} onChange={(event) => onChangeHandler(event, 'email')}/>
                        </label>
                        <label className={classes.auth_label}> Пароль
                            <input className={classes.auth_input} placeholder="Введите пароль" type="password" value={password} onChange={(event) => onChangeHandler(event, 'password')}/>
                        </label>
                        {isRegistration ? <label className={classes.auth_label}> Подтверждение пароля
                            <input className={classes.auth_input} placeholder="Введите пароль снова" type="password" value={pass} onChange={(event) => onChangeHandler(event, 'pass')}/>
                        </label> : ''}
                        {!isSamePasswords &&
                        isRegistration && !isFirstInput && <label className={classes.auth_pass_warning}> Пароли не совпадают </label>}
                        <div className={classes.auth_submit_container}>
                            <input type="submit" className={classes.auth_submit} value={isRegistration? 'Зарегистрироваться' : 'Войти'}/>
                        </div>
                        <Link to={isRegistration ? '/signup' : '/signin'} className={classes.auth_signin}>{ isRegistration ? 'Войти' : 'Зарегистрироваться' }</Link>
                    </form>
        </div>
    )
}

export default AuthForm;