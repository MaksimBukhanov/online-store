import React from "react";
import classes from './Footer.module.css';
import logo from '../../img/logo.png';
import {NavLink} from "react-router-dom";

const Footer = (props) => {
    let menuItemsFromServer = [
        {
            categoryName: 'Мужчины',
            link: '12'
        },
        {
            categoryName: 'Женщины',
            link: '/women'
        },
        {
            categoryName: 'Девочки',
            link: '/girls'
        },
        {
            categoryName: 'Мальчики',
            link: '/boys'
        }];
    let style = {
        background: `url(${logo})`,
        backgroundSize: 'contain',
        backgroundRepeat: 'no-'
    }
    return (
        <div className={classes.footer_main}>
            <div className={classes.footer_main_content}>
                <div className={classes.footer_content_left}>
                    <div className={classes.footer_logo} style={style}></div>
                    <div className={classes.footer_additional_text}>
                        <p className={classes.footer_text}>Работаем круглосуточно</p>
                        <p className={classes.footer_text}>Сделано в рамках дипломного проекта студентами КГУ</p>
                        <p className={classes.footer_text}>Смирнов Алексей, Буханов Максим 18-ВТбо-1</p>
                    </div>
                </div>
                <div className={classes.footer_content_center}>
                    <div className={classes.footer_menu}>
                        <div className={classes.footer_menu_items}>
                            <ul className={classes.footer_menu_list}>
                                {menuItemsFromServer.map(item => <div key={item.categoryName} className={classes.footer_menu_container}><NavLink
                                                                          to = {'/catalog' + item.link}
                                                                          className = {classes.footer_menu_item}
                                                                          key={item.categoryName}> {item.categoryName}
                                </NavLink> </div>)
                                }
                            </ul>
                        </div>
                    </div>
                </div>
                <div className={classes.footer_content_right}>
                    <div className={classes.footer_cards}>
                    </div>
                    <div className={classes.footer_rightinfo}>
                        <p className={classes.footer_text}>Служба технической поддержки</p>
                        <div className={classes.footer_supportinfo}>
                        <div className={classes.footer_telegram}> </div>
                        <div>Telegram @leshaold</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer;