import React from "react";
import classes from './Banner.module.css';
import Button from "../../Button/Button";

const Banner = (props) => {
    return (
        <div className={classes.banner_main}>
            <div className={classes.banner_main_layout}>
                <div className={classes.banner_main_layout_items}>
                    <div className={classes.banner_text_main}>BEAM WEAR</div>
                    <div className={classes.banner_text_additional}>Наша лучшая повседневная одежда</div>
                    <Button padding="5px 10px" margin="35px 0 0 0" link = '/catalog/man/clothes' caption = 'Посмотреть'/>
                </div>
            </div>
        </div>
    )
}

export default Banner;