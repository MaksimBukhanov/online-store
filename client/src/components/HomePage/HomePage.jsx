import React from "react";
import Banner from "./Banner/Banner";
import Categories from "./Categories/Categories";

const HomePage = (props) => {

    return (
        <div>
            <Banner/>
            <Categories/>
        </div>
    )
}

export default HomePage;