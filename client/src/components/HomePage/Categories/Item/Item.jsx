import React from "react";
import classes from './Item.module.css';
import Button from "../../../Button/Button";
import picture from '../../../../img/11255768-1.jpg'

const Item = (props) => {
    let styles = {
        background: `url(data:image/jpeg;base64,${props.image})`
    };
    if (props.mini) {
        return (
            <div className={classes.categories_item_mini}>
                <div className={classes.categories_item_image_mini} style={styles}></div>
                <div className={classes.categories_item_additional}>
                    <p className={classes.categories_item_name_mini}>{props.name}</p>
                    <div>
                    <Button link={'catalog' + props.link} width="84" height="19" padding='5px' caption="В каталог" radius="16px"/>
                    </div>
                </div>
            </div>
        )
    }
    else {
        return (
            <div className={classes.categories_item}>
                <div className={classes.categories_item_image} style={styles}></div>
                <div className={classes.categories_item_additional}>
                    <p className={classes.categories_item_name}>{props.name}</p>
                    <div>
                        <Button link={'catalog' + props.link} width="84" height="19" padding='5px 5px' caption="В каталог" radius="16px"/>
                    </div>
                </div>
            </div>
        )
    }

}

export default Item;