import React, {useEffect, useState} from "react";
import classes from './Categories.module.css';
import Item from "./Item/Item";
import axios from "axios";
import {menuItemsFromData} from "../../Header/helpers";
import {useDispatch, useSelector} from "react-redux";
import {setLoadingActionCreator} from "../../../redux/catalogReducer";
import Preloader from "../../Loader/Loader";

const Categories = (props) => {
    let [categories, setCategories] = useState([]);
    let isLoading = useSelector(state => state.catalog.isLoading);
    let dispatch = useDispatch();
    useEffect(() => {
        dispatch(setLoadingActionCreator(true));
        axios.get("http://185.7.84.163:8080/api/categories/top")
            .then(result => {
                setCategories(result.data)
                dispatch(setLoadingActionCreator(false))
            })
            .catch(e => console.log(e))
    }, []);
    if (isLoading) return <Preloader/>
    return (
          <div className={classes.categories_main}>
              <div className={classes.categories_main_title}><h1>Категории товаров</h1></div>
              {categories.length < 2 ? <div className={classes.categories_items}>
                  {categories.map(category => <Item key={category.name} name= {category.name} image={category.img} link={category.href}/>)}
              </div> : <div className={classes.categories_items}>
                  {categories.map((category, index) => index < 2 ? <Item key={category.name}  name= {category.name} link={category.href} image={category.img}/> : '')}
                  <div className={classes.categories_items_bottom}>
                      {categories.map((category, index) => index < 2 ? '' : <Item name= {category.name}  key={category.name} link={category.href} image={category.img} mini={true}/>)}
                  </div>
              </div>}
          </div>
    )
}

export default Categories;