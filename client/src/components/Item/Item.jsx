import React, {useEffect, useState} from "react";
import classes from './Item.module.css';
import picture from '../../img/11255768-1.jpg'
import Button from "../Button/Button";
import favActive from "../../img/favorites-red.png";
import favInactive from "../../img/favorites.svg";
import {useLocation, useNavigate, useParams} from "react-router-dom";
import axios from 'axios'
import Sizes from "../Catalog/Sizes/Sizes";
import Colors from "../Catalog/Colors/Colors";
import {useDispatch, useSelector} from "react-redux";
import API from "../../api/api";
import {Navigate} from "react-router";
import src from "../../img/11255768-1.jpg";
import {setLoadingActionCreator} from "../../redux/catalogReducer";
import Preloader from "../Loader/Loader";

const Item = (props) => {
    let location = useParams();
    let [info, setInfo] = useState([]);
    let [showError, setShowError] = useState(false);
    let [productVariant, setProductVariant] = useState(null);
    let navigate = useNavigate();
    let dispatch = useDispatch();
    let isLoading = useSelector(state => state.catalog.isLoading)
    let auth = useSelector(state => state.auth);
    useEffect(()=> {
        dispatch(setLoadingActionCreator(true));
        if (!auth.isAuth) {
            axios.get(`http://185.7.84.163:8080/api/catalog/product/${location.id}`)
                .then(res =>  {
                    setInfo(res.data)
                    dispatch(setLoadingActionCreator(false))
                })
        }
        else {
            API.get(`/api/catalog/product/${location.id}`)
                .then(res => {
                    setInfo(res.data)
                    dispatch(setLoadingActionCreator(false))
                    if (res.data.hasFavorites) {
                        changeStatus(true);
                    }
                });
        };
    }, [location.id])
    let [isAdded, changeAdded] = useState(false);
    let [status, changeStatus] = useState(false);
    const addToCart = (event) => {
        event.preventDefault();
        if (!isAdded && auth.isAuth) {
                if (!productVariant) {
                    setShowError(true);
                    return
                }
                API.post('/api/client/cart', {
                    productVariantId: productVariant,
                    count: 1
                }).then(res => {
                    changeAdded(true);
                    setShowError(false);
                })
         }
         else {
             navigate('/signup')
        }
    }
     const addToFav = (event) => {
        event.preventDefault();
        if (auth.isAuth) {
            if (!status) {
                API.post(`/api/client/favorites?productId=${location.id}`).then(res => changeStatus(true))
            }
            else {
                API.delete(`/api/client/favorites`, {data: [location.id]}).then(res => changeStatus(false))
            }
        }
        else {
            navigate('/signup')
        }
    }
    let items = [];
    if (isLoading) return <Preloader/>
    return (
      <div className={classes.item_container}>
            <h1 className={classes.item_title}> {info.name}</h1>
            <div className={classes.item_info}>
                <div className={classes.item_image_container}><img className={classes.item_image} src={info.img? `data:image/jpeg;base64, ${info.img}` : src}/></div>
                <div className={classes.item_additional}>
                    <div className={classes.item_price}>{info.price} ₽</div>
                    <div className={classes.item_desc}>Описание
                    <div className={classes.item_description}>Описание отстутствует</div>
                    </div>
                    <div className={classes.item_desc}>Размеры
                        <div className={classes.item_description}>
                            {info?.variants?.length ? info?.variants?.map(item => <div onClick={() => setProductVariant(item.id)}> <Sizes size={item.size}/></div>) : <div> Нет доступных размеров</div>}
                        </div>
                            {showError && info?.variants?.length ? <div className={classes.item_error}>Не указано значение!</div> : '' }
                    </div>
                    <div className={classes.item_desc}>Цвета
                        <div className={classes.item_description}>{info?.variants?.length ? info?.variants?.map(item  => {
                            if (!items.includes(item.color.id)) {
                                items.push(item.color.id)
                                return <Colors hex={item.color.hex}/>
                            }
                        }) : <div>Нет доступных цветов</div>}
                        </div>
                    </div>
                    <div className={classes.item_buyarea}>
                        <div className={classes.item_status} onClick={(event)=> addToFav(event)}>
                            {status ? <img className={classes.item_statusIcon} src={favActive}/> :
                                    <img className={classes.item_statusIcon} src={favInactive} />}
                        </div>
                    <div onClick={(event) => addToCart(event) } className={classes.item_buybutton}><Button caption = {isAdded ? 'Перейти в корзину' : info?.variants?.length ? 'Добавить в корзину' : 'Будет позже'} link= {isAdded && info.variants.length ? '/cart' : '#'}  width='443' padding="8px"/></div>
                    </div>
                </div>
            </div>
      </div>
    )
}

export default Item;