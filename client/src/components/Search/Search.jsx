import React, {useEffect} from "react";
import classes from './Search.module.css'
import Item from '../ProfileItems/View/Item/Item';
import {useDispatch, useSelector} from "react-redux";
import axios from "axios";
import API from "../../api/api";
import {setSearchItemsActionCreator} from "../../redux/ProfileItemsReducer";
import {setLoadingActionCreator} from "../../redux/catalogReducer";
import Preloader from "../Loader/Loader";


const Search = (props) => {
    let searchString = useSelector(state => state.header.searchString);
    let source = useSelector(state => state.profileItems.searchItems);
    let isAuth = useSelector(state => state.auth.isAuth)
    let isLoading = useSelector(state => state.catalog.isLoading);
    let dispatch = useDispatch();
    useEffect(()=> {
        dispatch(setLoadingActionCreator(true))
        if (isAuth) {
            API.post('/api/catalog/search', {
                word: searchString
            }).then(res => {
                dispatch(setSearchItemsActionCreator(res.data));
                dispatch(setLoadingActionCreator(false))
            });
        }
        else {
            axios.post('http://185.7.84.163:8080/api/catalog/search', {
                word: searchString
            }).then(res => {
                dispatch(setSearchItemsActionCreator(res.data));
                dispatch(setLoadingActionCreator(false));
            });
        }
    }, [searchString])
    if (isLoading) return <Preloader/>
    return (
        <div className={classes.search_container}>
            <div className={classes.search_main_title}><h1 className={classes.search_main_caption}>Результаты поиска для "{searchString}"</h1></div>
            <div className={classes.search_items}>
                {source?.map(item => <Item item={item} page="Поиск"/>)}
            </div>

        </div>
    )
}

export default Search;