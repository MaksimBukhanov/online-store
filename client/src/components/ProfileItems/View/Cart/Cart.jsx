import React, {useEffect, useState} from "react";
import classes from './../Favorites/Favorites.module.css';
import Item from '../Item/Item';
import API from "../../../../api/api";
import {useDispatch, useSelector} from "react-redux";
import {setCartItemsActionCreator, setDeleteItemActionCreator} from "../../../../redux/ProfileItemsReducer";
import Button from "../../../Button/Button";
import OrderInfo from "../../../Profile/OrderInfo/OrderInfo";
import {useNavigate} from "react-router-dom";
import {Navigate} from "react-router";
import {setLoadingActionCreator} from "../../../../redux/catalogReducer";

const Cart = (props) => {
    const dispatch = useDispatch();
    const items = useSelector((state) => state.profileItems.cartItems);
    const [selectedItems, setSelectedItems] = useState([]);
    const isAuth = useSelector(state => state.auth.isAuth);
    const userId = useSelector(state => state.auth.userId);
    const navigate = useNavigate();
    const isDeleting = useSelector((state => state.profileItems.isDeleting))
    const isLoading = useSelector(state => state.catalog.isLoading);
    useEffect(()=> {
        if (!items.length) {
            dispatch(setLoadingActionCreator(true))
            API.get(`/api/client/cart`)
                .then(res => {
                    dispatch(setCartItemsActionCreator(res.data));
                    dispatch(setLoadingActionCreator(false))
                });
        }
    },[])
    useEffect(()=> {
        if (!!isDeleting) {
            API.delete(`/api/client/cart`, {data: [isDeleting]})
                .then(res => dispatch(setCartItemsActionCreator(res.data)));
            dispatch(setDeleteItemActionCreator(''))
        }
    },[isDeleting])
    if (!isAuth) return  <Navigate to={'/signup'} replace/>
    if (!items.length) {
        return (
            <div className={classes.favorites_container_empty}>
                <div className={classes.favorites_container_empty_wrapper}>
                    <div className={classes.favorites_container_empty_title}>
                        <div className={classes.favorites_container_empty_image}></div>
                        Здесь пока ничего нет.</div>
                </div>
            </div>
        )
    }

    return (
        <div className={classes.favorites_container}>
            <div className={classes.favorites_main_title}><h1 className={classes.favorites_main_caption}>Корзина</h1></div>
            <form className={classes.favorites_items}>
                {items.map(item => <Item item={item} page="Корзина"/>)}
            </form>

        </div>
    )
}

export default Cart;