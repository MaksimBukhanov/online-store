import React, {useEffect} from "react";
import classes from './Favorites.module.css';
import Item from '../Item/Item';
import API from "../../../../api/api";
import {Navigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {setDeleteItemActionCreator, setFavItemsActionCreator} from "../../../../redux/ProfileItemsReducer";
import {setLoadingActionCreator} from "../../../../redux/catalogReducer";
import Preloader from "../../../Loader/Loader";

const Favorites = (props) => {
    const items = useSelector(state => state.profileItems.favItems);
    const dispatch = useDispatch();
    let isLoading = useSelector(state => state.catalog.isLoading);
    let isAuth = useSelector(state => state.auth.isAuth);
    const isDeleting = useSelector((state => state.profileItems.isDeleting))
    useEffect(()=> {
        if (!items.length) {
            dispatch(setLoadingActionCreator(true))
            API.get('/api/client/favorites')
                .then(res => {
                    dispatch(setFavItemsActionCreator(res.data));
                    dispatch(setLoadingActionCreator(false))
                })
        }
    },[])
    useEffect(()=> {
        if (!!isDeleting) {
            API.delete(`/api/client/favorites`, {data: [isDeleting]})
                .then(res => dispatch(setFavItemsActionCreator(res.data)));
            dispatch(setDeleteItemActionCreator(''))
        }
    },[isDeleting])
    if (!isAuth) return <Navigate to={'/signup'} replace/>
    if (isLoading) return <Preloader/>
    if (!items.length) {
        return (
            <div className={classes.favorites_container_empty}>
                <div className={classes.favorites_container_empty_wrapper}>
                    <div className={classes.favorites_container_empty_title}>
                        <div className={classes.favorites_container_empty_image}></div>
                        Здесь ничего нет. Но вы знаете, что делать</div>
                </div>
            </div>
        )
    }
    return (
        <div className={classes.favorites_container}>
            <div className={classes.favorites_main_title}><h1 className={classes.favorites_main_caption}>{props.title}</h1></div>
            <div className={classes.favorites_items}>
                {items.map(item => <Item item={item} page="Избранное"/>)}
            </div>
        </div>
    )
}

export default Favorites;