import React from "react";
import classes from './View.module.css';
import Item from "./Item/Item";

const View = (props) => {
    if (!props.items) {
        return (
            <div className={classes.favorites_container_empty}>
                <div className={classes.favorites_container_empty_wrapper}>
                    <div className={classes.favorites_container_empty_title}>
                        <div className={classes.favorites_container_empty_image}></div>
                        Здесь ничего нет. Но вы знаете, что делать</div>
                </div>
            </div>
        )
    }
    else return (
        <div className={classes.favorites_container}>
            <div className={classes.favorites_main_title}><h1 className={classes.favorites_main_caption}>{props.title}</h1></div>
            <div className={classes.favorites_items}>
                {props.items.map(item => <Item data={item}/>)}
            </div>
        </div>
    )
}

export default View;