import React, {createRef, useEffect, useState} from "react";
import classes from './MyAddresses.module.css';
import img from "../../../img/11255768-1.jpg";
import home from "../../../img/home.png";
import API from "../../../api/api";

const MyAddresses = (props) => {
    let [address, setAddress] = useState('');
    let [hasItems, setHasItems] = useState(false);
    let ref = createRef();
    useEffect( ()=> {
        setAddress(props.source);
        setHasItems(true);
    }, [props.source])
    let [isEnabled, setIsEnabled] = useState(false);
    const createAddress = () => {
        setAddress('');
        setHasItems(true);
        setIsEnabled(true);
    }
    let changeAddress = (event) => {
        event.preventDefault();
        setIsEnabled(true);
        ref.current.focus();
    }
    const addAddress = () => {
        API.patch('/api/client/address', {address})
            .then(res => setAddress(res.data.address))
    }
    let onSubmitHandler = (event) => {
        event.preventDefault();
            addAddress();
            setIsEnabled(false);
    }
    if (!hasItems) {
        return (
        <div className={classes.lk_address_empty} onClick={() => createAddress()}>Добавить адрес</div>
        )
    }
    return (
        <div className={classes.lk_my_address_info}>
            <div className={classes.lk_address_container} onClick={(e) => changeAddress(e)}>
                <div className={classes.lk_address_icon_container}>
                    <img className={classes.addresses_icon} src={home}/>
                </div>
                <div>
                    <form onSubmit={(e) => onSubmitHandler(e)}>
                    <input className={classes.lk_address_info} type="text" ref={ref} onChange={(e) => setAddress(e.currentTarget.value)} placeholder="Введите адрес" value={address} disabled={!isEnabled}/>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default MyAddresses;