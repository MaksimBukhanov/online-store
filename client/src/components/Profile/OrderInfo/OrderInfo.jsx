import classes from './OrderInfo.module.css';
import {useDispatch, useSelector} from "react-redux";
import picture from '../../../img/11255768-1.jpg'
import {setCartItemsActionCreator, setZakazInfoActionCreator} from "../../../redux/ProfileItemsReducer";
import Button from "../../Button/Button";
import API from "../../../api/api";
import {Link} from "react-router-dom";


const OrderInfo = (props) => {
    const dispatch = useDispatch();
    const userID = useSelector(state => state.auth.userId);
    let close = (event) => {
        event.preventDefault();
        dispatch(setZakazInfoActionCreator([]));
    }
    let onSubmit = (e) => {
        e.preventDefault();
        let products = props.source.map(item => {
            return {
                id: item.idVariant,
                count: 1
            }
        });
        API.post('/api/order/add', {
                client: userID,
                delivery: 1,
                products
        }).then(res => {
            dispatch(setZakazInfoActionCreator([]));
            API.delete(`/api/client/cart?clientId=${userID}`, {data: products.map(product => product.id)}).then(res => dispatch(setCartItemsActionCreator(res.data)))
        })
    }
    const items = props.source;
    return (
       <div className={classes.order_wrapper}>
           <div className={classes.order_header}>
               <div className={classes.order_title}>{props.action === "Оформление заказа" ? "Оформление заказа" : 'Информация о заказе'}</div>
               <div className={classes.order_closebtn} onClick={(event) => close(event)}>Закрыть</div>
           </div>
           <div className={classes.order_title}>{props.action === "Оформление заказа" ? "Выбранные товары" : ''}</div>
           {items.map(item => {
               return (
                   <Link className={classes.order_item_area} to={`/item/${item.id}`}>
                       <div className={classes.order_item}>
                               <div className={classes.order_img_area}><img className={classes.order_img} src = {`data:image/jpeg;base64, ${item.img}` || picture} /></div>
                                    <div className={classes.order_item_name}>{item.name}</div>
                                    <div>{item.size} размер</div>
                               <div className={classes.order_price}>{item.price.value || item.price}{item.price.currency || "Р"}</div>
                       </div>
                   </Link>
                   )
           })}
           {props.action === "Оформление заказа" ?
               <div>
                   <div className={classes.order_title}>Итого: {items[0]?.price?.value}{items[0]?.price?.currency}</div>
                   <iframe style={{marginLeft: '32px'}}
                       src='https://yoomoney.ru/quickpay/button-widget?targets=%D0%97%D0%B0%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80&default-sum=${}button-text=12&any-card-payment-type=on&button-size=m&button-color=white&successURL=&quickpay=small&account=41001880439387&'
                       width="184" height="36" frameBorder="0" allowTransparency="true" scrolling="no"></iframe>
                   <div onClick={(e) => onSubmit(e)}><Button caption="Оформить заказ" width="151" height="25" padding="10px" margin = "10px 0 0 32px" link="#"/></div>
               </div> : ''}
       </div>
    )
}

export default OrderInfo;