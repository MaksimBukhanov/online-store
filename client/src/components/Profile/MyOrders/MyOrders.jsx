import React, {useEffect, useState} from "react";
import classes from './MyOrders.module.css';
import API from "../../../api/api";
import {useDispatch} from "react-redux";
import {setZakazInfoActionCreator} from "../../../redux/ProfileItemsReducer";

const MyOrders = (props) => {
    const dispatch = useDispatch();
    let options = {
        day: 'numeric',
        month: 'long',
        year: 'numeric'
    }
    const date = new Date (props.source.date).toLocaleDateString('ru', options);
    let showInfo = (id) => {
        API.get(`/api/order/${id}`).then(res => dispatch(setZakazInfoActionCreator(res.data)))
    }
    return (
        <div className={classes.lk_my_order_orderarea} onClick={() => showInfo(props.source.id)}>
        <div className={classes.lk_my_orders_order_info}>
            <div>
                <div className={classes.lk_order_title}>Заказ {props.source.id} <div className={classes.lk_order_delivery}>{props.source.delivery} </div>
                </div>
           <span className={classes.lk_order_date}>от {date}</span> </div>
                <div className={classes.lk_order_status}>{props.source.status}</div>

        </div>
            <div className={classes.lk_order_button}>Посмотреть</div>
        </div>
    )
}

export default MyOrders;