import React from 'react'
import Loader from '../../img/loader.gif'
let Preloader = () =>
{
    return <div>
        <img style={{width: "100%", display: "flex", margin: "0 auto"}} src = {Loader}/>
    </div>
}
export default Preloader