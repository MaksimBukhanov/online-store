import React, {useEffect} from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Header from "./components/Header/Header";
import HomePage from "./components/HomePage/HomePage";
import Footer from "./components/Footer/Footer";
import AuthForm from "./components/Authetification/AuthForm/AuthForm";
import Profile from "./components/Profile/Profile";
import classes from './App.module.css'
import Catalog from "./components/Catalog/Catalog";
import Item from "./components/Item/Item";
import axios from "axios";
import jwt_decode from "jwt-decode";
import {setAuthActionCreator, setUserIdActionCreator} from "./redux/authReducer";
import {useDispatch, useSelector} from "react-redux";
import Favorites from "./components/ProfileItems/View/Favorites/Favorites";
import Cart from "./components/ProfileItems/View/Cart/Cart";
import Search from "./components/Search/Search";
import Admin from "./components/Admin/Admin";

const App = (props) => {
    const dispatch = useDispatch();
    let searchString = useSelector(state => state.header.searchString);
    let isAdmin = useSelector(state => state.auth.isAdmin);
    useEffect(()=> {
        if (localStorage.getItem('token') && localStorage.getItem('refreshToken')) {
            let refreshToken = localStorage.getItem('refreshToken');
            axios.post('http://185.7.84.163:8080/api/auth/token', {refreshToken})
                .then(response => {
                    if (response.data.accessToken) {
                        localStorage.setItem('token', response.data.accessToken)
                        const token = response.data.accessToken;
                        const info = jwt_decode(token);
                        dispatch(setUserIdActionCreator(+info.id));
                        dispatch(setAuthActionCreator(true))
                    }
                });
        }

    }, [])

    return (
        <BrowserRouter>
            <div className = "app_wrapper">
                {isAdmin ? <Admin/> :
                 <div>
                     <Header/>
                     <div className ={classes.app_wrapper_content}>
                     {searchString ? <Search/> :
                        <Routes>
                            <Route path = '/' element={<HomePage/>}/>
                            <Route path = '/favorites' element={<Favorites/>}/>
                            <Route path = '/cart' element={<Cart/>}/>
                            <Route path = '/search' element={<Search/>}/>
                            <Route path = '/signup' element={<AuthForm/>}/>
                            <Route path = '/signin' element={<AuthForm/>}/>
                            <Route path = '/profile' element={<Profile/>}/>
                            <Route path = '/catalog/:gender/' element={<Catalog/>}/>
                            <Route path = '/catalog/:gender/:type/' element={<Catalog/>}/>
                            <Route path = '/catalog/:gender/:type/:category/' element={<Catalog/>}/>
                            <Route path = '/item/:id' element={<Item/>}/>
                        </Routes>}
                </div>
                <Footer/>
                 </div>
                }
            </div>
        </BrowserRouter>
    );
}

export default App;
