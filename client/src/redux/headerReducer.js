

let initialState = {
    isShowing: false,
    searchString: null,
    counts: {
        fav: 0,
        cart: 0,
        profile: 0
    },
    currentValue: ''
};

let headerReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET-SHOWING": {
            let newState = {
                ...state,
                isShowing: action.data.flag,
                currentValue: action.data.flag? action.data.currentValue : state.currentValue
            }
            return newState;
        }
        case "SET-SEARCH" : {
            let newState = {
                ...state,
                searchString: action.searchString
            }
            return newState;
        }
        case "RESET-SEARCH" : {
            let newState = {
                ...state,
                searchString: null
            }
            return newState;
        }
        case "ADD-FAV": {
            let newState = {
                ...state,
                counts: {
                    ...state.counts,
                    fav: action.data.value? action.data.value: ++state.counts.fav
                }
            }
            return newState;
        }
        case "ADD-CART": {
            let newState = {
                ...state,
                counts: {
                    ...state.counts,
                    cart: action.value? action.data.value: ++state.counts.cart
                }
            }
            return newState;
        }
        case "UPDATE-PROFILE-COUNT": {
            let newState = {
                ...state,
                counts: {
                    ...state.counts,
                    profile: action.data.value? action.data.value: ++state.counts.profile
                }
            }
            return newState;
        }
        default: return state;
    }
}

export default headerReducer;

export const setShowingActionCreator = (info) => {
    let data = {flag: info.flag, currentValue: info.currentValue}
    return {type: "SET-SHOWING", data}
}
export const setProfileActionCreator = (value) => {
    return {type: "UPDATE-PROFILE-COUNT", value}
}
export const setCartActionCreator = (value) => {
    return {type: "ADD-CART", value}
}
export const setFavActionCreator = (value) => {
    return {type: "ADD-FAV", value}
}

export const setSearchActionCreator = (searchString) => {
    return {type: "SET-SEARCH", searchString}
}
export const resetSearchActionCreator = () => {
    return {type: "RESET-SEARCH"}
}