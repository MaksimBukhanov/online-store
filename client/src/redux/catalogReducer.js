let initialState = {
    subCategories: [],
    sizes: [],
    colors: [],
    filter: {
        colors: [],
        sizes: []
    },
    needReload: false,
    isEmp: true,
    detailItems: [],
    isLoading: false
};

let catalogReducer =  (state = initialState, action) => {
    switch (action.type) {
        case "SET-ITEMS": {
            let newState = {
                subCategories: action.items.type || state.subCategories,
                sizes: action.items.size || state.sizes,
                colors: action.items.color || state.colors,
            }
            return newState;
        }
        case "SET-RELOADING": {
            let newState = {
                ...state,
                needReloading: action.flag
            }
            return newState
        }
        case "SET-FILTER": {
            let newState = {
                ...state,
                filter: {
                    colors: action.items.color || state?.filter?.colors || [],
                    sizes: action.items.size || state?.filter?.sizes || []
                }
            }
            return newState;
        }
        case "SET-DETAIL": {
            let newState = {
                ...state,
                detailItems: action.detailItems
            }
            return newState
        }
        case "SET-EMPTY": {
            debugger;
            let newState = {
                ...state,
                isEmp: action.flag
            }
            return newState
        }
        case "SET-LOADING": {
            let newState = {
                ...state,
                isLoading: action.flag
            }
            return newState
        }
        default: return state;
    }
}

export default catalogReducer;

export const setItemsActionCreator = (items) => {
    return {type: 'SET-ITEMS', items}
}

export const setReloadingActionCreator = (flag) => {
    return {type: 'SET-RELOADING', flag}
}

export const setDetailActionCreator = (detailItems) => {
    return {type: 'SET-DETAIL', detailItems}
}

export const setEmptyActionCreator = (flag) => {
    return {type: 'SET-EMPTY', flag}
}

export const setFilterActionCreator = (items) => {
    return {type: 'SET-FILTER', items}
}

export const setLoadingActionCreator = (flag) => {
    return {type: "SET-LOADING", flag}
}