package com.beam.server.repository;

import com.beam.server.model.ProductClassEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IClazzRepository extends JpaRepository<ProductClassEntity, Long> {
}
