package com.beam.server.repository;

import com.beam.server.model.StatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IStatusRepository extends JpaRepository<StatusEntity, Long> {
}
