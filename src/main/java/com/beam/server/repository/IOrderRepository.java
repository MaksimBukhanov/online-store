package com.beam.server.repository;

import com.beam.server.model.OrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IOrderRepository extends JpaRepository<OrderEntity, Long>  {
    Page<OrderEntity> findAllByClient_Id (long id, Pageable pageable);
    List<OrderEntity> findAllByStatus_Id (long id);
    Optional<OrderEntity> findOrderEntityByIdAndClient_Id(long orderId, long clientId);
}
