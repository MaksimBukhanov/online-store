package com.beam.server.repository;

import com.beam.server.model.DeliveryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDeliveryRepository extends JpaRepository<DeliveryEntity, Long> {
}
