package com.beam.server.repository;

import com.beam.server.model.ColorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IProductColorRepository extends JpaRepository<ColorEntity, Integer> {
}
