package com.beam.server.repository;

import com.beam.server.model.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ICartRepository extends JpaRepository<CartEntity, Long> {
    List<CartEntity> findAllByClient_Id(long id);
    CartEntity findByClient_IdAndProduct_Id(long clientId, long productVariantId);
    @Transactional
    void deleteByClient_IdAndProduct_Id(long clientId, long productVariantId);
    long countAllByClient_Id(long clientId);
}
