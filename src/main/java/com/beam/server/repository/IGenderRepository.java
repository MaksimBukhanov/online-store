package com.beam.server.repository;

import com.beam.server.model.GenderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IGenderRepository extends JpaRepository<GenderEntity, Long> {
}
