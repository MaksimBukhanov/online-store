package com.beam.server.repository;

import com.beam.server.model.OrderBodyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IOrderBodyRepository extends JpaRepository<OrderBodyEntity, Long> {
    List<OrderBodyEntity> findAllByOrderEntity_Client_Id(Long id);
    List<OrderBodyEntity> findAllByOrderEntity_Id(Long id);
}
