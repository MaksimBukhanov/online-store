package com.beam.server.repository;

import com.beam.server.model.ProductTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IProductTypeRepository extends JpaRepository<ProductTypeEntity, Long> {
    List<ProductTypeEntity> findAllByGender_HrefAndClazz_Href(String gender, String clazz);
    List<ProductTypeEntity> findAllByGender_Href(String gender);
}
