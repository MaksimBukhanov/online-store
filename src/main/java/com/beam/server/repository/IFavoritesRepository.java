package com.beam.server.repository;

import com.beam.server.model.FavoritesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IFavoritesRepository extends JpaRepository<FavoritesEntity, Long> {
    List<FavoritesEntity> findAllByClient_Id(long id);
    FavoritesEntity findByClient_IdAndProduct_Id(long clientId, long productId);
    @Transactional
    void deleteByClient_IdAndProduct_Id(long clientId, long productId);
    long countAllByClient_Id(long clientId);
}

