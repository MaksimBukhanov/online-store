package com.beam.server.repository;

import com.beam.server.model.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IClientRepository extends JpaRepository<ClientEntity, Long> {
    int countAllByEmail(String email);
    ClientEntity getClientEntityByEmail(String email);
    List<ClientEntity> findAllByRole(long role);
}
