package com.beam.server.service;

import com.beam.server.model.OrderBodyEntity;

import java.util.List;

public interface IOrderBodyService {
    List<OrderBodyEntity> getAllOrderBodyByClientId(long id);
    void save(OrderBodyEntity entity);
    List<OrderBodyEntity> findAllByOrderId(long id);
}
