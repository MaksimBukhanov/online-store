package com.beam.server.service;

import com.beam.server.model.OrderEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface IOrderService {
    Page<OrderEntity> findAllOrderByClientId (long id, Pageable pageable);
    List<OrderEntity> findOrdersByStatusId (long id);
    Optional<OrderEntity> findOrderByOrderIdAndClientId (long orderID, long clientId);
    OrderEntity update (OrderEntity order);
    Optional<OrderEntity> findOrderById(long orderId);
    Page<OrderEntity> findAllOrder(Pageable pageable);
}
