package com.beam.server.service;

import com.beam.server.model.ProductVariantsEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.lang.Nullable;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class Utils {

    public static ObjectNode getJsonProductInfo(ObjectMapper mapper, ProductVariantsEntity product, long qty) {
        ObjectNode item = mapper.createObjectNode();
        item.put("id", product.getProduct().getId());
        item.put("idVariant", product.getId());
        item.put("name", product.getProduct().getName());
        ObjectNode price = mapper.createObjectNode();
        price.put("value", product.getProduct().getPrice());
        price.put("currency", "₽");
        item.set("price", price);
        item.put("size", product.getSize().getSize());
        item.put("img", Utils.getImgBase64(product.getImg()));
        ObjectNode color = mapper.createObjectNode();
        color.put("name", product.getColor().getName());
        color.put("hex", product.getColor().getHex());
        item.set("color", color);
        if (qty > 0)
            item.put("count", qty);
        item.put("total", product.getQty());
        return item;
    }
    public static String getStringBase64(byte[] img) {
        try {
            byte[] encodeBase64 = Base64.getEncoder().encode(img);
            String base64Encoded = new String(encodeBase64, "UTF-8");
            return base64Encoded;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
    public static byte[] getBytes(String stringBase64) {
        try {
            return Base64.getDecoder().decode(stringBase64.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getImgBase64 (@Nullable byte[] img) {
        if (img != null) {
            return Utils.getStringBase64(img);
        } else {
            return null;
        }
    }
}
