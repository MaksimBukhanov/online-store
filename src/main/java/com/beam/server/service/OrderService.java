package com.beam.server.service;

import com.beam.server.model.OrderEntity;
import com.beam.server.repository.IOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService implements IOrderService {

    @Autowired
    IOrderRepository orderRepository;

    @Override
    public Page<OrderEntity> findAllOrderByClientId(long id, Pageable pageable) {
        return orderRepository.findAllByClient_Id(id, pageable);
    }
    @Override
    public List<OrderEntity> findOrdersByStatusId(long id) {
        return orderRepository.findAllByStatus_Id(id);
    }
    @Override
    public Optional<OrderEntity> findOrderByOrderIdAndClientId(long orderId, long clientId) {
        return orderRepository.findOrderEntityByIdAndClient_Id(orderId, clientId);
    }
    @Override
    public OrderEntity update(OrderEntity order) {
        OrderEntity newOrder = orderRepository.save(order);
        return newOrder;
    }
    @Override
    public Optional<OrderEntity> findOrderById(long orderId) {
        return orderRepository.findById(orderId);
    }
    @Override
    public Page<OrderEntity> findAllOrder(Pageable pageable) {
        return orderRepository.findAll(pageable);
    }
}


