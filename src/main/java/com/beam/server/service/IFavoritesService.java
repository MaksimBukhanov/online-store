package com.beam.server.service;

import com.beam.server.model.FavoritesEntity;

import java.util.List;

public interface IFavoritesService {
    void addProduct(FavoritesEntity favoritesEntity);
    List<FavoritesEntity> findAllByClient(long id);
    FavoritesEntity findByClientAndProduct(long clientId, long productId);
    void deleteByClientAndProduct(long clientId, long productId);
    long countFavoritesByClient(long clientId);
}
