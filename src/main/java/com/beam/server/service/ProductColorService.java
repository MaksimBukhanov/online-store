package com.beam.server.service;

import com.beam.server.model.ColorEntity;
import com.beam.server.repository.IProductColorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductColorService implements IProductColorService {

    @Autowired
    IProductColorRepository productColorRepository;

    @Override
    public List<ColorEntity> getAllColor() {
        return productColorRepository.findAll();
    }
}
