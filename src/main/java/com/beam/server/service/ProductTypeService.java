package com.beam.server.service;

import com.beam.server.model.ProductTypeEntity;
import com.beam.server.repository.IProductTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductTypeService implements IProductTypeService {

    @Autowired
    IProductTypeRepository productTypeRepository;

    @Override
    public List<ProductTypeEntity> getAllProductTypeByGenderAndClazz(String gender, String clazz) {
        return productTypeRepository.findAllByGender_HrefAndClazz_Href(gender, clazz);
    }
    @Override
    public List<ProductTypeEntity> getAllProductTypeByGender(String gender) {
        return productTypeRepository.findAllByGender_Href(gender);
    }
    @Override
    public List<ProductTypeEntity> getAllProductType() {
        return productTypeRepository.findAll();
    }
    @Override
    public long getCountProductType() {
        return productTypeRepository.count();
    }
    @Override
    public Optional<ProductTypeEntity> getProductTypeEntityById(long id) {
        return productTypeRepository.findById(id);
    }
}
