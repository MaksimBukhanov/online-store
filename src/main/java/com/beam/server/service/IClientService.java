package com.beam.server.service;

import com.beam.server.model.ClientEntity;

import java.util.List;
import java.util.Optional;

public interface IClientService {
    void createClient(ClientEntity client);
    int countAllByEmail(String email);
    ClientEntity getClientByEmail(String email);
    Optional<ClientEntity> getClientById(long id);
    List<ClientEntity> findAllByRole(long id);
    void setAddress(ClientEntity client);
}
