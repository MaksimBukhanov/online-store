package com.beam.server.service;

import com.beam.server.model.OrderBodyEntity;
import com.beam.server.repository.IOrderBodyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderBodyService implements IOrderBodyService {

    @Autowired
    IOrderBodyRepository orderBodyRepository;

    @Override
    public List<OrderBodyEntity> getAllOrderBodyByClientId (long id) {
        return orderBodyRepository.findAllByOrderEntity_Client_Id(id);
    }
    @Override
    public void save(OrderBodyEntity entity) {
        orderBodyRepository.save(entity);
    }
    @Override
    public List<OrderBodyEntity> findAllByOrderId(long id) {
        return orderBodyRepository.findAllByOrderEntity_Id(id);
    }
}
