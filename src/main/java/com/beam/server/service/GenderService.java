package com.beam.server.service;

import com.beam.server.model.GenderEntity;
import com.beam.server.repository.IGenderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenderService implements IGenderService {

    @Autowired
    private IGenderRepository genderRepository;

    @Override
    public List<GenderEntity> findAll() {
        return genderRepository.findAll();
    }
}
