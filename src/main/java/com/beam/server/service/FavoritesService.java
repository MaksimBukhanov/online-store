package com.beam.server.service;

import com.beam.server.model.FavoritesEntity;
import com.beam.server.repository.IFavoritesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoritesService implements IFavoritesService {

    @Autowired
    private IFavoritesRepository favoritesRepository;

    @Override
    public void addProduct(FavoritesEntity favoritesEntity) {
        favoritesRepository.save(favoritesEntity);
    }
    @Override
    public List<FavoritesEntity> findAllByClient(long id) {
        return favoritesRepository.findAllByClient_Id(id);
    }
    @Override
    public FavoritesEntity findByClientAndProduct(long clientId, long productId) {
        return favoritesRepository.findByClient_IdAndProduct_Id(clientId, productId);
    }
    @Override
    public void deleteByClientAndProduct(long clientId, long productId) {
        favoritesRepository.deleteByClient_IdAndProduct_Id(clientId, productId);
    }
    @Override
    public long countFavoritesByClient(long clientId) {
        return favoritesRepository.countAllByClient_Id(clientId);
    }
}
