package com.beam.server.service;

import com.beam.server.model.GenderEntity;

import java.util.List;

public interface IGenderService {
    List<GenderEntity> findAll();
}
