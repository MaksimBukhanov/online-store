package com.beam.server.service;

import com.beam.server.model.ColorEntity;

import java.util.List;

public interface IProductColorService {
    List<ColorEntity> getAllColor();
}
