package com.beam.server.service;

import ch.qos.logback.core.net.server.Client;
import com.beam.server.model.ClientEntity;
import com.beam.server.repository.IClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService implements IClientService {
    @Autowired
    private IClientRepository clientRepository;
    @Override
    public void createClient(ClientEntity client)  {
        clientRepository.save(client);
    }
    @Override
    public int countAllByEmail(String email) {
        return clientRepository.countAllByEmail(email);
    }
    @Override
    public ClientEntity getClientByEmail(String email) {
        return clientRepository.getClientEntityByEmail(email);
    }
    @Override
    public Optional<ClientEntity> getClientById(long id) {
        return clientRepository.findById(id);
    }
    @Override
    public List<ClientEntity> findAllByRole(long id) {
        return clientRepository.findAllByRole(id);
    }
    @Override
    public void setAddress(ClientEntity client) {
        clientRepository.save(client);
    }
}
