package com.beam.server.service;

import com.beam.server.model.CartEntity;
import com.beam.server.repository.ICartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartService implements ICartService {
    @Autowired
    private ICartRepository cartRepository;

    @Override
    public void addProductToCart(CartEntity cartElement) {
        cartRepository.save(cartElement);
    }
    @Override
    public List<CartEntity> findAllByIdClient(long id) {
        return cartRepository.findAllByClient_Id(id);
    }
    @Override
    public CartEntity findByClientAndProduct(long clientId, long productVariantsId) {
        return cartRepository.findByClient_IdAndProduct_Id(clientId, productVariantsId);
    }
    @Override
    public void deleteByClientAndProduct(long clientId, long productVariantId) {
        cartRepository.deleteByClient_IdAndProduct_Id(clientId, productVariantId);
    }
    @Override
    public void updateCart(CartEntity cartEntity) {
        cartRepository.save(cartEntity);
    }
    @Override
    public long countCartByClient(long clientId) {
        return cartRepository.countAllByClient_Id(clientId);
    }
}
