package com.beam.server.service;

import com.beam.server.model.DeliveryEntity;

import java.util.List;
import java.util.Optional;

public interface IDeliveryService {
    Optional<DeliveryEntity> findById(long id);
    List<DeliveryEntity> findAll();
}
