package com.beam.server.service;

import com.beam.server.model.ProductEntity;
import com.beam.server.model.ProductVariantsEntity;
import com.beam.server.repository.IProductVariantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductVariantsService implements IProductVariantsService {

    @Autowired
    private IProductVariantRepository productVariantRepository;
    @Override
    public Optional<ProductVariantsEntity> findById(long id) {
        return productVariantRepository.findById(id);
    }
    @Override
    public void updateQty(ProductVariantsEntity entity) {
        productVariantRepository.save(entity);
    }
    @Override
    public Page<ProductVariantsEntity> findAll(Specification<ProductVariantsEntity> specification, Pageable pageable) {
        return productVariantRepository.findAll(specification, pageable);
    }
}
