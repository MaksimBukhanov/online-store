package com.beam.server.service;

import com.beam.server.model.SizeEntity;

import java.util.List;

public interface IProductSizeService {
    List<SizeEntity> getAllProductSizeByGenderAndClazz(String gender, String clazz);

    List<SizeEntity> getAllProductSizeByGender(String gender);
}
