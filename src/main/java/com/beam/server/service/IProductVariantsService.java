package com.beam.server.service;

import com.beam.server.model.ProductEntity;
import com.beam.server.model.ProductVariantsEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;

public interface IProductVariantsService {
    Optional<ProductVariantsEntity> findById(long id);
    void updateQty(ProductVariantsEntity entity);
    Page<ProductVariantsEntity> findAll(Specification<ProductVariantsEntity> specification, Pageable pageable);
}
