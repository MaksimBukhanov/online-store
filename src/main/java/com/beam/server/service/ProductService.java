package com.beam.server.service;

import com.beam.server.model.ProductEntity;
import com.beam.server.repository.IProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService implements IProductService{

    @Autowired
    private IProductRepository productRepository;

    @Override
    public Page<ProductEntity> findAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }
    @Override
    public Optional<ProductEntity> findById(long id) {
        return productRepository.findById(id);
    }
    @Override
    public List<ProductEntity> searchByWord(String word) {
        return productRepository.searchByWord(word);
    }
    @Override
    public Page<ProductEntity> findAll(Specification<ProductEntity> specification, Pageable pageable) {
        return productRepository.findAll(specification, pageable);
    }
    @Override
    public void save(ProductEntity product) {
        productRepository.save(product);
    }
}
