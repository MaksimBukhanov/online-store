package com.beam.server.rest;

import com.beam.server.domain.JwtAuthentication;
import com.beam.server.model.*;
import com.beam.server.service.*;
import com.beam.server.specifications.ProductSpecifications;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.yaml.snakeyaml.util.ArrayUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@RestController
@RequestMapping("/api/catalog")
@Api(tags = "/api/catalog")
@CrossOrigin
public class CatalogController {

    @Value("${jwt.secret.access}")
    private String accessSecret;
    @Value("${jwt.secret.refresh}")
    private String refreshSecret;
    private ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private ProductColorService productColorService;
    @Autowired
    private ProductTypeService productTypeService;
    @Autowired
    private ProductSizeService productSizeService;
    @Autowired
    private ProductService productService;
    @Autowired
    private AuthService authService;
    @Autowired
    private FavoritesService favoritesService;

    @ApiOperation(value = "Поиск по каталогу", notes = "Поиск по каталогу")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Запрос выполнен успешно")
    })
    @RequestMapping(value = "/search", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayNode> search (@RequestBody HashMap<String, String> word,
                                             @RequestHeader (name="Authorization", required = false) String token) {
        if (token != null)
            if (!validateToken(token))
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        List<ProductEntity> products = productService.searchByWord(word.get("word"));
        ArrayNode jsonProducts = mapper.createArrayNode();
        Boolean hasFavorites;
        String base64 = null;
        for (ProductEntity entity : products) {
            hasFavorites = null;
            hasFavorites = hasFavorites(token, hasFavorites, entity);
            ObjectNode item = mapper.createObjectNode();
            item.put("id", entity.getId());
            item.put("name", entity.getName());
            item.put("price", entity.getPrice());
            item.put("img", Utils.getImgBase64(entity.getImg()));
            item.put("hasFavorites", hasFavorites);
            jsonProducts.add(item);
        }
        return new ResponseEntity<>(jsonProducts, HttpStatus.OK);
    }

    @ApiOperation(value = "Получение товара по ID", notes = "Получение товара по ID")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Объекты не найдены в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> getProduct (@PathVariable Integer id,
                                                  @RequestHeader (name="Authorization", required = false) String token) {
        if (token != null)
            if (!validateToken(token))
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        Optional<ProductEntity> productEntityOptional = productService.findById(id);
        ProductEntity product = productEntityOptional.get();
        if (product == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ObjectNode jsonProduct = mapper.createObjectNode();
        ArrayNode variants = mapper.createArrayNode();
        jsonProduct.put("id", product.getId());
        jsonProduct.put("name", product.getName());
        jsonProduct.put("price", product.getPrice());
        jsonProduct.put("img",Utils.getImgBase64(product.getImg()));
        Boolean hasFavorites = null;
        for (ProductVariantsEntity entity : product.getProductVariants()) {
            if (!(entity.getQty() == 0)) {
                ObjectNode item = mapper.createObjectNode();
                ObjectNode color = mapper.createObjectNode();
                item.put("id", entity.getId());
                item.put("img", Utils.getImgBase64(entity.getImg()));
                item.put("qty", entity.getQty());
                color.put("id", entity.getColor().getId());
                color.put("name", entity.getColor().getName());
                color.put("hex", entity.getColor().getHex());
                item.set("color", color);
                item.put("size", entity.getSize().getSize());
                variants.add(item);
                if (hasFavorites == null)
                    hasFavorites = hasFavorites(token, hasFavorites, entity.getProduct());
            }
        }
        jsonProduct.set("variants", variants);
        jsonProduct.put("hasFavorites", hasFavorites);
        return new ResponseEntity<>(jsonProduct, HttpStatus.OK);
    }

    @ApiOperation(value = "Получение каталога товаров (Постранично с сортировкой)", notes = "Получение каталога товаров (Постранично с сортировкой)")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Объекты не найдены в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @RequestMapping(value = "/products", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> getAllProducts(@RequestParam("page")int page,
                                                    @RequestParam("size") int size,
                                                    @RequestParam("gender") Long genderId,
                                                    @RequestParam(name = "type", required = false) Long typeId,
                                                    @RequestParam(name = "class") Long classId,
                                                    @RequestParam(name = "sort", required = false) String sort,
                                                    @RequestBody(required = false) HashMap<String, List<Long>> filters) {
        if (page < 0 || size < 1)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Pageable pageable = null;
        if (sort == null) {
            pageable = PageRequest.of(page, size);
        } else if (sort.equals("asc")) {
            pageable = PageRequest.of(page, size, Sort.by("price").ascending());
        } else if (sort.equals("desc")) {
            pageable = PageRequest.of(page, size, Sort.by("price").descending());
        }
        List<Long> sizes = filters.get("sizes");
        List<Long> colors = filters.get("colors");
        Page<ProductEntity> pageProducts;
        Specification<ProductEntity> specification;
        if (typeId == null && sizes.isEmpty() && colors.isEmpty()) {
            specification = ProductSpecifications.hasGenderId(genderId)
                    .and(ProductSpecifications.hasClazzId(classId));
        } else if (typeId != null && sizes.isEmpty() && colors.isEmpty()) {
            specification = ProductSpecifications.hasGenderId(genderId)
                    .and(ProductSpecifications.hasClazzId(classId))
                    .and(ProductSpecifications.hasTypeId(typeId));
        } else if (typeId == null && !sizes.isEmpty() && colors.isEmpty()) {
            specification = ProductSpecifications.hasGenderId(genderId)
                    .and(ProductSpecifications.hasClazzId(classId))
                    .and(ProductSpecifications.hasSizesId(sizes));
        } else if (typeId == null && sizes.isEmpty() && !colors.isEmpty()) {
            specification = ProductSpecifications.hasGenderId(genderId)
                    .and(ProductSpecifications.hasClazzId(classId))
                    .and(ProductSpecifications.hasColorsId(colors));
        } else if (typeId != null && !sizes.isEmpty() && colors.isEmpty()) {
           specification = ProductSpecifications.hasGenderId(genderId)
                    .and(ProductSpecifications.hasClazzId(classId))
                    .and(ProductSpecifications.hasTypeId(typeId))
                    .and(ProductSpecifications.hasSizesId(sizes));
        } else if (typeId == null && !sizes.isEmpty() && !colors.isEmpty()) {
           specification = ProductSpecifications.hasGenderId(genderId)
                    .and(ProductSpecifications.hasClazzId(classId))
                    .and(ProductSpecifications.hasSizesId(sizes))
                    .and(ProductSpecifications.hasColorsId(colors));
        } else if (typeId != null && sizes.isEmpty() && !colors.isEmpty()) {
           specification = ProductSpecifications.hasGenderId(genderId)
                    .and(ProductSpecifications.hasClazzId(classId))
                    .and(ProductSpecifications.hasTypeId(typeId))
                    .and(ProductSpecifications.hasColorsId(colors));
        } else {
            specification = ProductSpecifications.hasGenderId(genderId)
                    .and(ProductSpecifications.hasClazzId(classId))
                    .and(ProductSpecifications.hasTypeId(typeId))
                    .and(ProductSpecifications.hasSizesId(sizes))
                    .and(ProductSpecifications.hasColorsId(colors));
        }
        pageProducts = productService.findAll(specification, pageable);
        ArrayNode products = mapper.createArrayNode();
        List<ProductEntity> pageProductsList = new ArrayList<>();
        ObjectNode json = mapper.createObjectNode();
        if (pageProducts != null)
            pageProductsList = pageProducts.stream().toList();
        if (pageProductsList.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        for (ProductEntity entity : pageProductsList) {
            ObjectNode item = mapper.createObjectNode();
            item.put("id", entity.getId());
            item.put("name", entity.getName());
            item.put("price", entity.getPrice());
            item.put("img", Utils.getImgBase64(entity.getImg()));
            products.add(item);
        }
        json.put("totalPage", pageProducts.getTotalPages());
        json.set("page", products);
        return new ResponseEntity<>(json, HttpStatus.OK);
    }
    @ApiOperation(value = "Получение фильтров", notes = "Получение фильтров")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Объекты не найдены в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @RequestMapping(value = "/filter", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> getFilters(@RequestParam String gender,
                                                 @RequestParam(required = false) String clazz) {
        ObjectNode responseJson = mapper.createObjectNode();
        List<ColorEntity> allColor = productColorService.getAllColor();
        List<ProductTypeEntity> productTypeList;
        List<SizeEntity> allSize;
        gender = "/" + gender;
        if (clazz == null) {
            productTypeList = productTypeService.getAllProductTypeByGender(gender);
            allSize = productSizeService.getAllProductSizeByGender(gender);
        } else {
            clazz = "/" + clazz;
            productTypeList = productTypeService.getAllProductTypeByGenderAndClazz(gender, clazz);
            allSize = productSizeService.getAllProductSizeByGenderAndClazz(gender, clazz);
        }
        if (productTypeList.isEmpty() || allColor.isEmpty() || allSize.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ArrayNode tempArray = mapper.createArrayNode();
        for (ProductTypeEntity entity : productTypeList) {
            ObjectNode item = mapper.createObjectNode();
            item.put("id", entity.getName().getId());
            item.put("name", entity.getName().getName());
            item.put("href", entity.getClazz().getHref() +
                    entity.getName().getHref());
            tempArray.add(item);
        }
        responseJson.set("type", mapper.valueToTree(tempArray));
        tempArray.removeAll();
        for (ColorEntity entity : allColor) {
            ObjectNode item = mapper.createObjectNode();
            item.put("id", entity.getId());
            item.put("name", entity.getName());
            item.put("hex", entity.getHex());
            tempArray.add(item);
        }
        responseJson.set("color", mapper.valueToTree(tempArray));
        tempArray.removeAll();
        for (SizeEntity entity : allSize) {
            ObjectNode item = mapper.createObjectNode();
            item.put("id", entity.getId());
            item.put("size" ,entity.getSize());
            tempArray.add(item);
        }
        responseJson.set("size", mapper.valueToTree(tempArray));
        return new ResponseEntity<>(responseJson, HttpStatus.OK);
    }

    private Boolean hasFavorites(String token, Boolean hasFavorites, ProductEntity entity) {
        if (token != null && hasFavorites == null) {
            JwtAuthentication authInfo = authService.getAuthInfo();
            long clientId = Long.valueOf(authInfo.getId());
            if (favoritesService.findByClientAndProduct(clientId, entity.getId()) != null) {
                return true;
            } else {
                return false;
            }
        }
        return null;
    }
    private boolean validateToken(String token) {
        JwtProvider jwtProvider = new JwtProvider(accessSecret, refreshSecret);
        if (StringUtils.hasText(token) && token.startsWith("Bearer ")) {
            token =  token.substring(7);
            return jwtProvider.validateAccessToken(token);
        }
        return false;
    }
}
