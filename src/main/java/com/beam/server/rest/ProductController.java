package com.beam.server.rest;

import com.beam.server.model.ProductEntity;
import com.beam.server.service.ProductService;
import com.beam.server.service.Utils;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@Slf4j
@RestController
@RequestMapping("/api/product")
@Api(tags = "/api/product")
@CrossOrigin
public class ProductController {

    @Autowired
    ProductService productService;

    @ApiOperation(value = "Добавление новог товара", notes = "Добавление новог товара")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Объекты не найдены в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
   // @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayNode> addProduct(@RequestBody HashMap<String, String> base64) {
        byte[] bytes = Utils.getBytes(base64.get("img"));
        ProductEntity product = productService.findById(2).get();
        product.setImg(bytes);
        productService.save(product);
        return null;
    }

    @ApiOperation(value = "Изменение характеристик товара", notes = "Изменение характеристик товара")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Объекты не найдены в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/change", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayNode> changeProduct() {
        return null;
    }

    @ApiOperation(value = "Удаление товара", notes = "Удаление товара")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Объекты не найдены в БД"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно"),
    })
    //@PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayNode> deleteProduct() {
        return null;
    }
}
