package com.beam.server.rest;

import com.beam.server.domain.JwtAuthentication;
import com.beam.server.model.*;
import com.beam.server.service.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/order")
@Api(tags = "/api/order")
@CrossOrigin
public class OrderController {
    private ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private OrderService orderService;
    @Autowired
    private StatusService statusService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private DeliveryService deliveryService;
    @Autowired
    private ProductVariantsService productVariantsService;
    @Autowired
    private OrderBodyService orderBodyService;
    @Autowired
    private AuthService authService;

    @ApiOperation(value = "Получение информации о заказе (список товаров) пользователя", notes = "Получение информации о заказах (список товаров) пользователя")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученных данных"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно")
    })
    @RequestMapping(value = "/{orderId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayNode> getOrder (@PathVariable Long orderId) {
        final JwtAuthentication authInfo = authService.getAuthInfo();
        long clientId = Long.valueOf(authInfo.getId());
        Optional<OrderEntity> orderEntityOptional = orderService.findOrderByOrderIdAndClientId(orderId, clientId);
        OrderEntity orderEntity = orderEntityOptional.get();
        if (orderEntity == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ArrayNode products = mapper.createArrayNode();
        List<OrderBodyEntity> orderBodyEntities = orderBodyService.findAllByOrderId(orderEntity.getId());
        for (OrderBodyEntity entity : orderBodyEntities) {
            ObjectNode item = mapper.createObjectNode();
            item.put("id", entity.getProduct().getProduct().getId());
            item.put("name", entity.getProduct().getProduct().getName());
            item.put("price", entity.getProduct().getProduct().getPrice());
            item.put("count", entity.getQty());
            item.put("img", Utils.getImgBase64(entity.getProduct().getImg()));
            ObjectNode color = mapper.createObjectNode();
            color.put("name", entity.getProduct().getColor().getName());
            color.put("hex", entity.getProduct().getColor().getName());
            item.set("color", color);
            item.put("size", entity.getProduct().getSize().getSize());
            products.add(item);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @ApiOperation(value = "Изменение статуса заказа", notes = "Изменение статуса заказа")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученных данных"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/status", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> changeStatus (@RequestParam Long orderId,
                                                   @RequestParam Long statusId,
                                                   @RequestParam Integer page) {
        Optional<OrderEntity> orderEntityOptional = orderService.findOrderById(orderId);
        OrderEntity order = orderEntityOptional.get();
        if (order == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Optional<StatusEntity> statusEntityOptional = statusService.findById(statusId);
        StatusEntity status = statusEntityOptional.get();
        if (status == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        order.setStatus(status);
        return getOrder(page);
    }

    @ApiOperation(value = "Изменение сотрудника, ответственного за заказ", notes = "Изменение сотрудника, ответственного за заказ")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученных данных"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/employee", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> changeEmployee (@RequestParam Long orderId,
                                                     @RequestParam String email,
                                                     @RequestParam Integer page) {
        ClientEntity employee = clientService.getClientByEmail(email);
        if (employee.getRole() == 1 || employee == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Optional<OrderEntity> orderEntityOptional = orderService.findOrderById(orderId);
        OrderEntity order = orderEntityOptional.get();
        if (order == null)
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        order.setEmployee(employee);
        return getOrder(page);
    }

    @ApiOperation(value = "Получение списка заказов", notes = "Получение списка заказов")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученных данных"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно")
    })
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> getOrder (@RequestParam Integer page) {
        if (page < 0)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Pageable pageable = PageRequest.of(page, 6);
        Page<OrderEntity> ordersPage = orderService.findAllOrder(pageable);
        List<OrderEntity> orders = new ArrayList<>();
        if (ordersPage != null)
            orders = ordersPage.stream().toList();
        if (orders.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ArrayNode ordersJson = mapper.createArrayNode();
        for (OrderEntity order : orders) {
            ObjectNode item = mapper.createObjectNode();
            ObjectNode status = mapper.createObjectNode();
            item.put("id", order.getId());
            item.put("date", String.valueOf(order.getDate()));
            item.put("clientId", order.getClient().getId());
            item.put("delivery", order.getDelivery().getName());
            item.put("employee", order.getEmployee().getEmail());
            status.put("id", order.getStatus().getId());
            status.put("name", order.getStatus().getName());
            item.set("status", status);
            ordersJson.add(item);
        }
        ObjectNode json = mapper.createObjectNode();
        json.put("totalPage", ordersPage.getTotalPages() - 1);
        json.set("orders", ordersJson);
        return new ResponseEntity<>(json, HttpStatus.OK);
    }

    @ApiOperation(value = "Создание нового заказа", notes = "Создание нового заказа")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученных данных"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно")
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ObjectNode> addOrder (@RequestBody ObjectNode orderJson) {
        OrderEntity newOrder = new OrderEntity();
        Optional<StatusEntity> statusEntityOptional = statusService.findById(1);
        Optional<ClientEntity> clientEntityOptional =
                clientService.getClientById(orderJson.path("client").asLong());
        Optional<DeliveryEntity> deliveryEntityOptional =
                deliveryService.findById(orderJson.path("delivery").asLong());
        if (statusEntityOptional.isEmpty() || clientEntityOptional.isEmpty() ||
                deliveryEntityOptional.isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        newOrder.setDelivery(deliveryEntityOptional.get());
        newOrder.setClient(clientEntityOptional.get());
        newOrder.setStatus(statusEntityOptional.get());
        newOrder.setDate(new Date(System.currentTimeMillis()));
        newOrder = orderService.update(newOrder);

        ArrayNode products = (ArrayNode) orderJson.path("products");
        List<OrderBodyEntity> orderBodyEntities = new ArrayList<>();
        for (JsonNode item : products) {
            OrderBodyEntity newOrderBody = new OrderBodyEntity();
            int count = item.path("count").asInt();
            Optional<ProductVariantsEntity> productVariantsEntityOptional =
                    productVariantsService.findById(item.path("id").asLong());
            if (productVariantsEntityOptional.isEmpty())
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            ProductVariantsEntity productVariantsEntity = productVariantsEntityOptional.get();
            if (count > productVariantsEntity.getQty())
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            productVariantsEntity.setQty(productVariantsEntity.getQty() - count);
            productVariantsService.updateQty(productVariantsEntity);
            newOrderBody.setQty(count);
            newOrderBody.setProduct(productVariantsEntityOptional.get());
            newOrderBody.setOrderEntity(newOrder);
            orderBodyEntities.add(newOrderBody);
            orderBodyService.save(newOrderBody);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "Все способы доставки", notes = "Все способы доставки")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Ошибка в полученных данных"),
            @ApiResponse(code = 200, message = "Запрос выполнен успешно")
    })
    @PreAuthorize("hasAuthority('CLIENT')")
    @RequestMapping(value = "/delivery", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DeliveryEntity> getAllDelivery () {
        return deliveryService.findAll();
    }
}
