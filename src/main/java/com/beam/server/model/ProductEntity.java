package com.beam.server.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@Entity
@Table(name = "product", schema = "store")
public class ProductEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "price", nullable = false, precision = 8, scale = 2)
    private double price;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "img", nullable = false)
    private byte[] img;

    @ManyToOne
    @JoinColumn(name = "type", referencedColumnName = "id", nullable = false)
    private ProductTypeEntity type;

    @OneToMany(mappedBy = "product")
    private Collection<ProductVariantsEntity> productVariants;
}
