package com.beam.server.specifications;

import com.beam.server.model.ProductEntity;
import com.beam.server.model.ProductTypeEntity;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;

import java.util.List;

public class ProductSpecifications {

    public static Specification<ProductEntity> hasGenderId(long gender) {
        return (root, query, cb) -> {
            Join<ProductEntity, ProductTypeEntity> table = root.join("type");
            query.distinct(true);
            return cb.equal(table.<Long>get("gender"), gender);
        };
    }
    public static Specification<ProductEntity> hasClazzId(long clazz) {
        return (root, query, cb) -> {
            Join<ProductEntity, ProductTypeEntity> table = root.join("type");
            query.distinct(true);
            return cb.equal(table.<Long>get("clazz"), clazz);
        };
    }
    public static Specification<ProductEntity> hasTypeId(long type) {
        return (root, query, cb) -> {
            query.distinct(true);
            return cb.equal(root.<Long>get("type"), type);
        };
    }
    public static Specification<ProductEntity> hasSizesId(List<Long> sizes) {
        return (root, query, cb) -> {
            query.distinct(true);
            Join productVariants = (Join) root.fetch("productVariants");
            return productVariants.get("size").in(sizes);
        };
    }
    public static Specification<ProductEntity> hasColorsId(List<Long> colors) {
        return (root, query, cb) -> {
            query.distinct(true);
            Join productVariants = (Join) root.fetch("productVariants");
            return productVariants.get("color").in(colors);
        };
    }
}
