ALTER TABLE store."product_type" ADD CONSTRAINT "product_type_fk0" FOREIGN KEY ("gender") REFERENCES store."gender"("id");
ALTER TABLE store."product_type" ADD CONSTRAINT "product_type_fk1" FOREIGN KEY ("class") REFERENCES store."product_class"("id");
ALTER TABLE store."product_type" ADD CONSTRAINT "product_type_fk2" FOREIGN KEY ("name") REFERENCES store."categories"(id);

ALTER TABLE store."product" ADD CONSTRAINT "product_fk2" FOREIGN KEY ("type") REFERENCES store."product_type"("id");

ALTER TABLE store."orders" ADD CONSTRAINT "orders_fk0" FOREIGN KEY ("client") REFERENCES store."client"("id");
ALTER TABLE store."orders" ADD CONSTRAINT "orders_fk2" FOREIGN KEY ("delivery") REFERENCES store."delivery"("id");
ALTER TABLE store."orders" ADD CONSTRAINT "orders_fk3" FOREIGN KEY ("employee") REFERENCES store."client"("id");
ALTER TABLE store."orders" ADD CONSTRAINT "orders_fk4" FOREIGN KEY ("status") REFERENCES store."status"("id");

ALTER TABLE store."order_body" ADD CONSTRAINT "order_body_fk0" FOREIGN KEY ("product") REFERENCES store."product_variants"("id");
ALTER TABLE store."order_body" ADD CONSTRAINT "order_body_fk1" FOREIGN KEY ("orders") REFERENCES store."orders"("id");

ALTER TABLE store."product_variants" ADD CONSTRAINT "product_variants_fk0" FOREIGN KEY ("product") REFERENCES store."product"("id");
ALTER TABLE store."product_variants" ADD CONSTRAINT "product_variants_fk1" FOREIGN KEY ("color") REFERENCES store."color"("id");
ALTER TABLE store."product_variants" ADD CONSTRAINT "product_variants_fk2" FOREIGN KEY ("size") REFERENCES store."size"("id");

ALTER TABLE store."size" ADD CONSTRAINT "product_size_fk0" FOREIGN KEY ("gender") REFERENCES store."gender"("id");
ALTER TABLE store."size" ADD CONSTRAINT "product_size_fk1" FOREIGN KEY ("class") REFERENCES store."product_class"("id");

ALTER TABLE store."cart" ADD CONSTRAINT "cart_fk0" FOREIGN KEY ("client") REFERENCES store."client"("id");
ALTER TABLE store."cart" ADD CONSTRAINT "cart_fk1" FOREIGN KEY ("product") REFERENCES store."product_variants"("id");

ALTER TABLE store."client" ADD CONSTRAINT "client_fk0" FOREIGN KEY ("role") REFERENCES store."role"("id");

ALTER TABLE store."favorites" ADD CONSTRAINT "favorites_fk0" FOREIGN KEY ("client") REFERENCES store."client"("id");
ALTER TABLE store."favorites" ADD CONSTRAINT "favorites_fk1" FOREIGN KEY ("product") REFERENCES store."product"("id");