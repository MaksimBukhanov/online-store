CREATE SCHEMA store;

CREATE TABLE store."client" (
	"id" bigserial,
	"full_name" character varying NOT NULL,
	"address" character varying,
	"email" character varying NOT NULL UNIQUE,
	"phone" character varying NOT NULL UNIQUE,
	"password" character varying NOT NULL,
	"role" bigint NOT NULL,
	CONSTRAINT "client_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."role" (
    "id" bigserial,
    "name" character varying NOT NULL UNIQUE,
    CONSTRAINT "role_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."color" (
	"id" bigserial,
	"name" character varying NOT NULL UNIQUE,
	"hex" character varying NOT NULL UNIQUE,
	CONSTRAINT "color_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."categories" (
    "id" bigserial,
    "name" character varying NOT NULL,
    "href" character varying NOT NULL,
    CONSTRAINT "categories_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."product_type" (
	"id" bigserial,
	"name" bigint NOT NULL,
	"img" bytea,
	"gender" bigint NOT NULL,
	"class" bigint NOT NULL,
	CONSTRAINT "product_type_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."gender" (
	"id" bigserial,
	"name" character varying NOT NULL UNIQUE,
	"href" character  varying NOT NULL UNIQUE,
	CONSTRAINT "gender_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."product_class" (
	"id" bigserial,
	"name" character varying NOT NULL UNIQUE,
	"href" character  varying UNIQUE,
	CONSTRAINT "product_class_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."product" (
	"id" bigserial,
	"name" character varying NOT NULL,
	"type" bigint NOT NULL,
	"price" double precision NOT NULL,
    "img" bytea,
	CONSTRAINT "product_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."orders" (
	"id" bigserial,
	"client" bigint NOT NULL,
	"date" DATE NOT NULL,
	"delivery" bigint NOT NULL,
	"employee" bigint,
	"status" bigint NOT NULL,
	CONSTRAINT "orders_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."status" (
	"id" bigserial,
	"name" character varying NOT NULL UNIQUE,
	CONSTRAINT "status_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."delivery" (
	"id" bigserial,
	"name" character varying NOT NULL UNIQUE,
	"price" double precision NOT NULL,
	CONSTRAINT "delivery_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."order_body" (
	"id" bigserial,
	"product" bigint NOT NULL,
	"qty" integer NOT NULL,
	"orders" bigint NOT NULL,
	CONSTRAINT "order_body_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."product_variants" (
	"id" bigserial,
	"product" bigint NOT NULL,
	"color" bigint NOT NULL,
	"size" bigint NOT NULL,
	"qty" integer,
    "img" bytea,
	CONSTRAINT "product_variants_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."size" (
	"id" bigserial,
	"size" character varying NOT NULL,
	"gender" bigint NOT NULL,
	"class" bigint NOT NULL,
	CONSTRAINT "size_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."cart" (
	"id" bigserial,
	"client" bigint NOT NULL,
    "product" bigint NOT NULL,
    "qty" integer NOT NULL,
	CONSTRAINT "cart_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE store."favorites" (
    "id" bigserial,
    "client" bigint NOT NULL,
    "product" bigint NOT NULL,
    CONSTRAINT "favorites_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);